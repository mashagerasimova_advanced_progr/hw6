﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW6_MashaGerasimova
{
    public partial class Form2 : Form
    {
        private string birthdays;
        const int year = 2;

        public Form2(string userPath)
        {
            this.birthdays = userPath;
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }


        private void mnthClndraBirthdays_DateChanged(object sender, DateRangeEventArgs e) // Click on date on the calendar event
        {
            string[] lines= System.IO.File.ReadAllLines(this.birthdays); // Reading from the data file
            mnthClndrBirthdays.MaxSelectionCount = 1;
            string selectedDate = mnthClndrBirthdays.SelectionRange.Start.ToShortDateString(); // Getting the selected date from the calendar in DD.MM.YYYY format
            string[] splittedSelDate = selectedDate.Split('.'); // Selected date to check
            bool flag = false;
            //int i = 0;
            foreach (string line in lines) // Comparing the selected date to each date in file
            {
                flag = false;
                string[] splittedInfo = line.Split(','); // Splitting the current line
                string[] splittedBirth = splittedInfo[1].Split('/'); // Date from file to check

                // Comparing the dates fields to each other
                if (Int32.Parse(splittedSelDate[0]) == Int32.Parse(splittedBirth[1]) && Int32.Parse(splittedSelDate[1]) == Int32.Parse(splittedBirth[0]) && Int32.Parse(splittedSelDate[year]) == Int32.Parse(splittedBirth[year]))
                {
                    flag = true;
                }
                if (flag) // If the selected date exists in the data file
                {
                    // Printing info about the date to user
                    txtInfo.Text = "On the selected date "  + splittedInfo[0] + " celebrates birthday!";
                    break;
                }
                else
                {
                    // Printing info about the date to user
                    txtInfo.Text = "No one has birthday on the chosen date";
                }
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e) // Click on 'X' button event
        {
            if (string.Equals((sender as Button).Name, @"CloseButton"))
            {
                Form1 f1 = new Form1();
                f1.Form1_FormClosing(sender, e);
                this.Close();
                Application.Exit();
            }
        }
    }
}
