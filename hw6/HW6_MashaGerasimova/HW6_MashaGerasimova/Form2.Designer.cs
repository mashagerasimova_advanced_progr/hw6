﻿namespace HW6_MashaGerasimova
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnthClndrBirthdays = new System.Windows.Forms.MonthCalendar();
            this.txtInfo = new System.Windows.Forms.Label();
            this.txtChoose = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mnthClndrBirthdays
            // 
            this.mnthClndrBirthdays.Location = new System.Drawing.Point(104, 40);
            this.mnthClndrBirthdays.Name = "mnthClndraBirthdays";
            this.mnthClndrBirthdays.TabIndex = 0;
            this.mnthClndrBirthdays.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.mnthClndraBirthdays_DateChanged);
            // 
            // txtInfo
            // 
            this.txtInfo.AutoSize = true;
            this.txtInfo.Location = new System.Drawing.Point(85, 211);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(197, 13);
            this.txtInfo.TabIndex = 1;
            this.txtInfo.Text = "No one has birthday on the chosen date";
            // 
            // txtChoose
            // 
            this.txtChoose.AutoSize = true;
            this.txtChoose.Location = new System.Drawing.Point(134, 18);
            this.txtChoose.Name = "txtChoose";
            this.txtChoose.Size = new System.Drawing.Size(115, 13);
            this.txtChoose.TabIndex = 2;
            this.txtChoose.Text = "Choose date to check:";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 261);
            this.Controls.Add(this.txtChoose);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.mnthClndrBirthdays);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar mnthClndrBirthdays;
        private System.Windows.Forms.Label txtInfo;
        private System.Windows.Forms.Label txtChoose;
    }
}