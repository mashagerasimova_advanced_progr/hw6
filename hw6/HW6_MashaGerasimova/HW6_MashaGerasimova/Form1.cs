﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace HW6_MashaGerasimova
{
    public partial class Form1 : Form
    {
        const string usersPath = @"C:\Users\kfir\Desktop\user\Users.txt";
        const string magshimPath = @"C:\Users\kfir\Desktop\user\MagshimBD.txt";
        const string lidorPath = @"C:\Users\kfir\Desktop\user\%E2%80%8F%E2%80%8FLidorBD.txt";

        public Form1()
        {
            InitializeComponent();
            //Form wnd = new MainScreen();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e) // Click on 'Cancel' button event
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e) // Click on 'Log in' button event
        {
            bool flag = false;
            string[] lines;
            if (!txtBxLogin.Text.Equals("") && !txtBxPassword.Text.Equals("")) // If the user did enter his data
            {
                lines = System.IO.File.ReadAllLines(usersPath); // Reading from the Users.txt file
                foreach (string line in lines) // Checking log in
                {
                    if (line.Contains(txtBxLogin.Text) && line.Contains(txtBxPassword.Text)) // User exists
                    {
                        flag = true;
                    }
                }
                if (flag) // If user exists
                {
                    // Checking what exactly is the user to use a right data file after
                    if (lidorPath.Contains(txtBxLogin.Text)) // Lidor's file path contains the login text
                    {
                        Form2 ins = new Form2(lidorPath); // Creating other window and sending the path
                        this.Hide();
                        ins.Show();
                    }
                    else if (magshimPath.Contains(txtBxLogin.Text)) // Magshim's file path contains the login text
                    {
                        Form2 ins = new Form2(magshimPath); // Creating other window and sending the path
                        this.Hide();
                        ins.Show();
                    }      
                }       
            }   
        }

        public void Form1_FormClosing(object sender, FormClosingEventArgs e) // Click on 'X' button event
        {
            if (string.Equals((sender as Button).Name, @"CloseButton"))
            {
                this.Close();
                Application.Exit();
            }
        }
    }

}
